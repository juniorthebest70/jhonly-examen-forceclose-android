package com.jjgarcia.examenjhonly.model
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class CursoApiService {

    private val baseUrlCurso = "http://www.mocky.io/v2/"
    private val apiServiceCurso = Retrofit.Builder()
        .baseUrl(baseUrlCurso)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(EstadoApi::class.java)

    fun getEstado() : Single<Estado>{
        return apiServiceCurso.getEstado("465")
    }

    fun getCursos() : Single<List<Cursos>>{
        return apiServiceCurso.getCursos("465")
    }

    fun getCursoDetalle(curso_codigo : String) : Single<CursoDetalle>{
        return  apiServiceCurso.getCursoDetalle(curso_codigo)
    }

    fun setMatricula(curso_codigo : String) : Single<Matricula>{
        val request  = RequestMAtricula("456",curso_codigo)
        return  apiServiceCurso.getMatricula(request)
    }



}