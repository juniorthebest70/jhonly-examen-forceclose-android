package com.jjgarcia.examenjhonly.model

import android.media.MediaMetadataRetriever
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.*

interface EstadoApi {

    @GET("5e52d42b2d0000261d357bd9/")
    fun getEstado(@Query("user_id") user_id : String): Single<Estado>

    @GET("5e52d5832d00006300357bdb/")
    fun getCursos(@Query("user_id") user_id : String): Single<List<Cursos>>

    @GET("5e52d6682d00004c00357bdf/")
    fun getCursoDetalle(@Query("curso_codigo") curso_codigo : String): Single<CursoDetalle>

    @POST("5e52d6b42d00007100357be0/")
    fun getMatricula(@Body params: RequestMAtricula): Single<Matricula>

}