package com.jjgarcia.examenjhonly.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.jjgarcia.examenjhonly.R
import com.jjgarcia.examenjhonly.model.Cursos
import kotlinx.android.synthetic.main.item_cursos.view.*

class CursosListAdapter(private val cursosList : ArrayList<Cursos>) : RecyclerView.Adapter<CursosListAdapter.CursosViewHolder>() {

    class CursosViewHolder(var view: View) : RecyclerView.ViewHolder(view)

    fun updateCursos(newCursosList : List<Cursos>){
        cursosList.clear()
        cursosList.addAll(newCursosList)
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CursosViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_cursos,parent,false)
        return CursosViewHolder(view)
    }

    override fun getItemCount() = cursosList.size

    override fun onBindViewHolder(holder: CursosViewHolder, position: Int) {

        holder.view.cursoname.text = cursosList[position].código + " " +  cursosList[position].curso
        holder.view.cursoLayout.setOnClickListener {
            val direction = cursosDirections.actionCursosFragmentToMatricula3(cursosList[position])
            Navigation.findNavController(holder.view).navigate(direction)
        }
    }
}