package com.jjgarcia.examenjhonly.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager

import com.jjgarcia.examenjhonly.R
import com.jjgarcia.examenjhonly.ViewModel.CursosViewModel
import com.jjgarcia.examenjhonly.model.Cursos
import kotlinx.android.synthetic.main.fragment_cursos.*

class cursos : Fragment() {

    private lateinit var viewmodelCursos : CursosViewModel
    private val listAdapter = CursosListAdapter(arrayListOf())

    private val activeObserver = Observer <String>{activo ->
        Log.d("Esta LLamando","hasta que mande cursos")
        if (activo == "0"){
            edtestado.text = "Inactivo"
        }else{
            edtestado.text = "Activo"
        }
    }

    private val cursosListObserver = Observer<List<Cursos>>{cursos ->

        cursos?.let {
            cursosList.visibility = View.VISIBLE
            listAdapter.updateCursos(cursos)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cursos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cursosList.apply {
            layoutManager = GridLayoutManager(context,1)
            adapter = listAdapter
        }
    }

    override fun onStart() {
        super.onStart()
        viewmodelCursos = ViewModelProvider(this).get(CursosViewModel::class.java)
        viewmodelCursos.getEstado()
        viewmodelCursos.getcursos()
        viewmodelCursos.estado.observe(this,activeObserver)
        viewmodelCursos.cursos.observe(this,cursosListObserver)

    }


}
