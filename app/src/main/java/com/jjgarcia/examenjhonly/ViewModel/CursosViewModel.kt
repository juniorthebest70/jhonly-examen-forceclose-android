package com.jjgarcia.examenjhonly.ViewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.jjgarcia.examenjhonly.model.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class CursosViewModel(application: Application) : AndroidViewModel(application) {

    val cursos by lazy { MutableLiveData<List<Cursos>>() }
    val estado by lazy { MutableLiveData<String>() }
    val loaderror by lazy { MutableLiveData<Boolean>() }
    val loading by lazy { MutableLiveData<Boolean>() }
    val cursoDetalle by lazy { MutableLiveData<CursoDetalle>() }
    val matricula by lazy { MutableLiveData<Matricula>() }

    private val disposable = CompositeDisposable()
    private val apiService = CursoApiService()

    fun getEstado() {
        disposable.add(
            apiService.getEstado()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Estado>() {
                    override fun onSuccess(it: Estado) {
                        Log.d("Estado -->", it.estado)
                        estado.value = it.estado
                    }

                    override fun onError(e: Throwable) {
                        Log.d("Error", e.localizedMessage)
                    }
                })
        )
    }

    fun getEstadoPooling() {

        loading.value = true
        val seconds = Observable.interval(5, TimeUnit.SECONDS)
        val disposable1 = seconds.subscribe { l -> logData() }
        disposable.addAll(disposable1)
        Log.d("Test", ("All Disposed!"))
    }

    private fun logData() {
        disposable.add(
            apiService.getEstado()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Estado>() {
                    override fun onSuccess(it: Estado) {
                        Log.d("Estado -->", it.estado)
                        estado.value = it.estado
                    }

                    override fun onError(e: Throwable) {
                        Log.d("Error", e.localizedMessage)
                    }
                })
        )
    }

    fun getcursos() {
        disposable.add(
            apiService.getCursos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Cursos>>() {
                    override fun onSuccess(it: List<Cursos>) {
                        Log.d("Cursos --->", it.toString())
                        loaderror.value = false
                        loading.value = false
                        cursos.value = it
                    }

                    override fun onError(e: Throwable) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
        )
    }

    fun getCursoDetalle(curso_codigo: String) {
        disposable.add(
            apiService.getCursoDetalle(curso_codigo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<CursoDetalle>() {
                    override fun onSuccess(t: CursoDetalle) {
                        cursoDetalle.value = t
                    }

                    override fun onError(e: Throwable) {
                        Log.d("Error getCursoDetalle", e.localizedMessage)
                    }

                })
        )
    }


    fun setMatricula(curso_codigo : String) {
        disposable.add(
            apiService.setMatricula(curso_codigo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Matricula>() {
                    override fun onSuccess(it: Matricula) {
                        Log.d("matricula --->", it.toString())
                        matricula.value = it
                    }

                    override fun onError(e: Throwable) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}